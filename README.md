Dungeoning
==========
About
-----
A Rougue-Like game made in 7 days for the 7DRL Challenge, in dungeoning you control
a group of skelletons inside a dungeon floor, trying to survive the invading heroes

Dear diary
----------
Day 0 (03 March 2017): Had the ideia and setup basic stuff, will strat coding tomorrow

Day 1 (04 March 2017): Implemented map generation, camera and sprites, will be using p5

Day 2 (05 March 2017): Improved map generation, Implemented pathfinding and entities

Day 3 (06 March 2017): Fixed a couple of bugs, Refactored some code, Implemented basic controls

Day 4 (07 March 2017): ... (Had too much to do IRL to work on the game =( )

Day 5 (08 March 2017): Added Enemies, coins and basic AI